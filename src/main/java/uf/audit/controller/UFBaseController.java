package uf.audit.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.core.Controller;
import com.jfinal.kit.PropKit;
import com.jfinal.upload.MultipartRequest;

import org.piaohao.fast.jfinal.Util;
import uf.audit.util.AesUtil;
import uf.audit.util.Consts;

/**
 * 基础Controller，提供必要的json（Angular）支持
 * 
 * @author sunny
 *
 */
public class UFBaseController extends Controller {
	protected String getMethod() {
		return getRequest().getMethod();
	}

	static Properties prop = new Properties();
	static {

		/**
		 attach=/home/azureuser/audittestattach
		 iterationCount=1000
		 keySize=128
		 salt=e9f5dd179ca0c8e7d5bd0a3aa633629a
		 iv=ebf56d08aa6c9f13ccccc010c3e184b9
		 */

		InputStream in = UFBaseController.class.getClassLoader().getResourceAsStream("sysinfo.properties");
		try {
			prop.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Boolean initedAESEnable = null;

	protected boolean enableAES() {
		// iterationCount=1000
		// keySize=128
		// salt=e9f5dd179ca0c8e7d5bd0a3aa633629a
		// iv=ebf56d08aa6c9f13ccccc010c3e184b9
		// passphrase=123456
		if (initedAESEnable == null) {
			initedAESEnable = new Boolean(
					(prop.getProperty("iterationCount") != null) && (prop.getProperty("keySize") != null)
							&& (prop.getProperty("salt") != null) && (prop.getProperty("iv") != null));
		}
		return initedAESEnable.booleanValue();
	}

	protected String getPostData() {
		InputStream is;
		try {
			is = getRequest().getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, Consts.DEFAULT_ENCODING));
			StringBuilder sb = new StringBuilder();
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (!enableAES()) {
				return sb.toString();
			} else {
				String ciphertext = sb.toString();
				String passphrase = AesUtil.PASSPHRASE;
				int iterationCount = Integer.parseInt(prop.getProperty("iterationCount"));
				int keySize = Integer.parseInt(prop.getProperty("keySize"));
				String salt = prop.getProperty("salt");
				String iv = prop.getProperty("iv");
				AesUtil aesUtil = new AesUtil(keySize, iterationCount);
				String plaintext = aesUtil.decrypt(salt, iv, passphrase, ciphertext);
				return plaintext;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getMultiPara(String field) {
		HttpServletRequest request = getRequest();
		MultipartRequest mreq = null;
		if (request instanceof MultipartRequest == false)
			mreq = new MultipartRequest(request);
		else
			mreq = (MultipartRequest) request;
		return mreq.getParameter(field);
	}

	protected void renderSuccess() {
		renderJson("{\"status\":\"ok\"}");
	}

	protected void renderError() {
		renderJson("{\"status\":\"error\"}");
	}

	protected void renderError(String errMsg) {
		renderJson("{\"status\":\"error\", \"msg\":\"" + errMsg + "\"}");
	}
}

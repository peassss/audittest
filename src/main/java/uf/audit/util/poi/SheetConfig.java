package uf.audit.util.poi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SheetConfig {
	
	private List<String> headerList;
	private List<String> columnList;
	private Map<String, String> importColumnMap;
	private List<Integer> columnWidthList;
	
	public List<String> getHeaderList() {
		return headerList;
	}
	public void addHeader(String displayName) {
		if(this.headerList == null){
			this.headerList = new ArrayList<String>();
		}
		this.headerList.add(displayName);
	}
	
	public List<String> getColumnList() {
		return columnList;
	}
	public void addColumn(String columnName) {
		if(this.columnList == null){
			this.columnList = new ArrayList<String>();
		}
		this.columnList.add(columnName);
	}
	
	public Map<String, String> getImportColumnMap() {
		return importColumnMap;
	}
	public void addImportColumnMap(String displayName, String columnName) {
		if(this.importColumnMap == null){
			this.importColumnMap = new HashMap<String, String>();
		}
		this.importColumnMap.put(displayName, columnName);
	}
	
	public List<Integer> getColumnWidthList() {
		return columnWidthList;
	}
	public void addColumnWidth(int columnWidth) {
		if(this.columnWidthList == null){
			this.columnWidthList = new ArrayList<Integer>();
		}
		this.columnWidthList.add(columnWidth);
	}
	
	public boolean checkValid(){
		if(this.headerList == null || this.columnList == null || this.headerList.size() != this.columnList.size()){
			return false;
		}
		return true;
	}
	
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		if(this.headerList != null && !this.headerList.isEmpty()){
			buffer.append("this.headerList = ");
			for(int index = 0, length = this.headerList.size(); index < length; index++){
				buffer.append("|").append(this.headerList.get(index));
			}
			buffer.append("\n");
		}
		if(this.columnList != null && !this.columnList.isEmpty()){
			buffer.append("this.columnList = ");
			for(int index = 0, length = this.columnList.size(); index < length; index++){
				buffer.append("|").append(this.columnList.get(index));
			}
			buffer.append("\n");
		}
		if(this.importColumnMap != null && !this.importColumnMap.isEmpty()){
			buffer.append("this.importColumnMap = ");
			for(Iterator<String> it = this.importColumnMap.keySet().iterator(); it.hasNext(); ){
				String key = it.next();
				buffer.append("|").append(key).append("=").append(this.importColumnMap.get(key));
			}
			buffer.append("\n");
		}
		if(this.columnWidthList != null && !this.columnWidthList.isEmpty()){
			buffer.append("this.columnWidthList = ");
			for(int index = 0, length = this.columnWidthList.size(); index < length; index++){
				buffer.append("|").append(this.columnWidthList.get(index));
			}
			buffer.append("\n");
		}
		
		return buffer.toString();
	}

}

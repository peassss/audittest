package uf.audit.db;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.json.FastJson;
import com.jfinal.json.Json;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import redis.clients.jedis.Jedis;
import uf.audit.util.Table;

import java.util.*;

/**
 * 试卷答案数据实体
 *
 * @author sunny
 */
@Table(key = "testoptbh")
public class TestOpt extends Model<TestOpt> {
    private static final long serialVersionUID = -7644540082576539659L;
    public static TestOpt dao = new TestOpt().use(DbKit.MAIN_CONFIG_NAME);
    //定义全局的jedis
    public Jedis jedis = new Jedis("localhost");
    //存储所有选项
    public Map<String, String> subjects = new HashMap<String, String>();

    // 容错处理 如果set的值为空，就不做处理
    public TestOpt set(String attr, Object value) {
        if (value != null) {
            super.set(attr, value);
        }
        return this;
    }

    // 删除指定试卷的答案
    public void deleteWithTestBh(String testbh) {
        DbPro.use().update("delete from testopt where testbh=?", testbh);
    }

    // 获取某个试题所有的答案选项
    public List<TestOpt> getoptions(String testbh) {
        return find("select * from testopt where testbh=?", testbh);
    }

    /*
     * opt.testbh = item.testbh; opt.itembh = item.itembh; opt.optbh =
     * item.optbh; opt.answer = item.answer;
     */
    // 更新
    public boolean update(JSONObject obj) {
        String bh = obj.getString("testoptbh");
        TestOpt item = new TestOpt();
        item.set("testbh", obj.getString("testbh"));
        item.set("itembh", obj.getString("itembh"));
        item.set("answer", obj.getString("answer"));
        item.set("optbh", obj.getString("optbh"));
        boolean result = false;
        if (bh != null) {
            item.set("testoptbh", bh);
            result = item.update();
        } else {
            item.set("testoptbh", UUID.randomUUID().toString());
            result = item.save();
        }
        return result;
    }

    //将所有答案放入redis
    public boolean updateRedis(JSONObject obj, String testbh, String userbh, int size) {
        String bh = obj.getString("testoptbh");
        TestOpt item = new TestOpt();
        item.set("testbh", obj.getString("testbh"));
        item.set("itembh", obj.getString("itembh"));
        item.set("answer", obj.getString("answer"));
        item.set("optbh", obj.getString("optbh"));
        if (bh != null) {
            item.set("testoptbh", bh);
            subjects.put(obj.getString("optbh"), item.toJson());
            if (subjects.size() == size) {
                jedis.hmset(userbh + "^" + testbh + "^answer", subjects);
                subjects.clear();
            }
            return true;
        } else {
            item.set("testoptbh", UUID.randomUUID().toString());
            subjects.put(obj.getString("optbh"), item.toJson());
            if (subjects.size() == 4) {
                jedis.hmset(userbh + "^" + testbh + "^answer", subjects);
                subjects.clear();
            }
            return true;
        }
    }

    // 更新部分
    public boolean updateSimple(JSONObject obj) {
        String bh = obj.getString("testoptbh");
        TestOpt item = new TestOpt();
        boolean result = false;
        if (bh != null) {
            item.set("testoptbh", bh);
            item.set("ext", obj.getString("ext"));
            result = item.update();
        }
        return result;
    }

    // 删除
    public boolean delete(String bh) {
        return deleteById(bh);
    }

    //改变redis中所有已经答题的状态
    public boolean updateState(String testbh, String userbh) {
        String key = "" + userbh + "^" + testbh;
        String is_exist = jedis.lrange(key, 0, 100).get(0);
        JSONArray jsonArray = JSONArray.parseArray(is_exist);
        //获取缓存中的改变的item
        Set<String> set = jedis.smembers(userbh + "^" + testbh + "change");
        for (String str : set) {
            JSONArray jsonArray1 = JSONArray.parseArray(is_exist);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i).getJSONObject("columns");
                Record record = new Record().setColumns(FastJson.getJson().parse(jsonObject.toJSONString(), Map.class));
                if (record.getStr("optbh").equals(str)) {
                    record.set("answer", "1");
                    jsonArray.set(i, record);
                } else if (record.getStr("answer") == null || !record.getStr("answer").equals("1")) {
                    record.set("answer", "0");
                    jsonArray.set(i, record);
                }
            }
        }
        String s = jsonArray.toJSONString();
        jedis.del(key);
        jedis.lpush(key, s);
        return true;
    }

    public void updateTour(String userbh, String testbh, JSONObject arr) {
        Integer sortno = (Integer) arr.get("itembh") + 1;
        Map<String, String> map = jedis.hgetAll("tourist");
        String jsonstr = map.get("sort" + sortno);
        JSONArray jsonArray = JSONArray.parseArray(jsonstr);
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i).getJSONObject("columns");
            Record record = new Record().setColumns(FastJson.getJson().parse(jsonObject.toJSONString(), Map.class));
            if (record.getStr("optbh").equals(arr.getString("optbh"))) {
                record.set("answer", "1");
                jsonArray.set(i, record);
            }
        }
        map.put("sort" + sortno, jsonArray.toJSONString());
        jedis.del("tourist");
        jedis.hmset("tourist", map);
    }
}

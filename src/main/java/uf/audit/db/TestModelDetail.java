package uf.audit.db;

import java.util.List;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Model;

import uf.audit.util.Table;

/**
 * 试卷模板明细实体
 * 
 * @author sunny
 *
 */
@Table(key = "detailbh")
public class TestModelDetail extends Model<TestModelDetail> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static TestModelDetail dao = new TestModelDetail().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public TestModelDetail set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取指定模板的明细信息
	public List<TestModelDetail> getmodeldetails(String modelbh) {
		return find("select * from testmodeldetail where modelbh=?", modelbh);
	}

	// 删除指定模板的明细信息
	public void deleteWithModelbh(String modelbh) {
		DbPro.use().update("delete from testmodeldetail where modelbh=?", modelbh);
	}

	// 更新
	public boolean update(JSONObject obj) {
		String bh = obj.getString("detailbh");
		TestModelDetail item = new TestModelDetail();
		item.set("modelbh", obj.getString("modelbh"));
		item.set("seqbh", obj.getString("seqbh"));
		item.set("catebh", obj.getString("catebh"));
		item.set("itemtype", obj.getString("itemtype"));
		item.set("itemcount", obj.getInteger("itemcount"));
		item.set("score", obj.getInteger("score"));
		item.set("itemlevel", obj.getInteger("itemlevel"));
		boolean result = false;
		if (bh != null) {
			item.set("detailbh", obj.getString("detailbh"));
			result = item.update();
		} else {
			item.set("detailbh", UUID.randomUUID().toString());
			result = item.save();
		}
		return result;
	}

	// 删除
	public boolean delete(String bh) {
		return deleteById(bh);
	}
}

package uf.audit.db;

import java.util.List;

import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

import uf.audit.util.Table;

/**
 * 考题数据实体
 * 
 * @author sunny
 *
 */
@Table(key = "testquestionbh")
public class TestQuestion extends Model<TestQuestion> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static TestQuestion dao = new TestQuestion().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public TestQuestion set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 删除指定试卷的试题
	public void deleteWithTestBh(String testbh) {
		DbPro.use().update("delete from testquestion where testbh=? ", testbh);
	}

	// 获取指定试题的答案信息
	public List<TestQuestion> getquestions(String testbh) {
		return find("select * from testquestion where testbh=?", testbh);
	}

	// 获取某个试卷试题数量
	public int getquestioncount(String testbh) {
		String sql = "select count(*) as cnt from testquestion where testbh=?";
		Record rec = DbPro.use().findFirst(sql, testbh);
		return rec.getLong("cnt").intValue();
	}

	// 删除
	public boolean delete(String bh) {
		return deleteById(bh);
	}
}

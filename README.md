# 用友审计考试系统

1、开发环境：
- Java version "1.7.0_79"
- Apache Maven 3.1.1
- Eclipse Luna(4.4.2 JavaEE) 或 IDEA 2018
- nodejs v8.9.4
- npm v5.6.0
- gulp v3.9.1


2、步骤
- 运行 `npm install` 完成前端脚本打包所需依赖的下载（仅第一次）
- 运行 `gulp` 开始打包前端脚本和资源
- 运行 `mvn clean package appassembler:assemble -DskipTests=true` 可以完成打包工作（第一次会比较耗时），生成的项目：`target\AuditTest\`
- 运行`bin/AuditTest-1.0.0.bat`直接运行（默认端口`8080`）
- 通过浏览器访问考试系统
> 数据部分的准备请看后面的要求

3、代码结构
后端代码：src/main/java
后端依赖的第三方包在pom中有详细定义
这里采用了**JFinal**作为后端框架，在这里一并感谢

代码说明
* uf.audit.db：数据库访问实体
* uf.audit.db.support：数据库连接池插件
* uf.audit.config：jfinal配置类
* uf.audit.controller：控制器（后端逻辑）
* uf.audit.intercept：拦截器（权限及登录拦截）
* uf.audit.util：工具支持类

前端代码：src/main/webapp
第三方js库（js/lib）：angularjs（1.2.29最低兼容IE8）、jQuery（1.9）、Amaze（1.0.1）
应用逻辑（js/app）：login.js（登录），index.js（主页）

4、代码构建
* 后端是标准的maven工程，直接以maven工程方式导入到IDE即可,使用`mvn package`编译打包
* 前端基于`gulp`构建，需要安装`node`，`npm`工具，然后在根目录下运行`npm isntall -g gulp gulp-cli`及`gulp`完成前端代码的打包
* 使用`mvn clean package appassembler:assemble -DskipTests=true` 打包系统
* 进入 `target/AuditTest/bin` 下面，启动`AuditTest-1.0.0.bat` 即可

5、前置准备
> 建立数据库`audittest`，然后导入`data.sql`文件，根据您实际情况修改`db.properties`文件中数据连接信息
> 配置文件：src/main/resources，目前有日志（log4j.properties）和数据库（db.properties）配置文件

6、其他说明
* 如果导入eclipse后没有显示为dynamic web项目，请先做facets操作，注意dynamic web module为2.3，java选1.5，javascript选1.0。
* IDEA下不需做任何处理
* 初始账户信息: admin/admin

**Snapshot**
<img src="http://oeax46h54.bkt.clouddn.com/snapshot.png"/>
<img src="http://oeax46h54.bkt.clouddn.com/snapshot2.png"/>

> 北京用友审计软件有限公司一直使用该系统进行内部职级定级考试（题库不公开 :) ）

**MIT**协议，可以复制，分发和传播

